<?php

namespace Ringo;

class Crest
{

    private $__client_id = null;
    private $__secret_key = null;
    private $__calback_url = null;
    private $__token = null;
    private $__basic_auth = null;

    public $auth_url = "https://login.eveonline.com/oauth/authorize/";
    public $token_url = "https://login.eveonline.com/oauth/token";
    public $verify_url = 'https://login.eveonline.com/oauth/verify';
    public $decode_url = 'https://crest-tq.eveonline.com/decode/';

    public function __construct($client_id, $secret_key, $callback_url)
    {
        $this->__client_id = $client_id;
        $this->__secret_key = $secret_key;
        $this->__calback_url = $callback_url;
        $this->__basic_auth = base64_encode($client_id . ":" . $secret_key);
    }

    public function get_token($code)
    {
        $data = array(
            'grant_type' => 'authorization_code',
            'code' => $code
        );

        $context = $this->prepare_request($data, "Authorization: Basic {$this->__basic_auth}");
        $result = file_get_contents($this->token_url, false, $context);

        if ($result) {
            $result = json_decode($result, true);
            if (isset($result['error'])) {
                ; // Deal with error;
            } else {
                $this->__token = $result;
            }
        }
        return $this->__token;
    }

    public function set_token($new_token)
    {
        $this->__token = $new_token;
    }

    public function refresh_token($refresh_token=null)
    {
        if (!$refresh_token) {
            $refresh_token = $this->__token['refresh_token'];
        }
        $data = [
            "grant_type" => "refresh_token",
            "refresh_token" => $refresh_token
        ];
        $context = $this->prepare_request($data, "Authorization: Basic {$this->__basic_auth}");
        $result = file_get_contents($this->token_url, false, $context);
        if ($result) {
            return json_decode($result, true);
        }
    }

    public function verify_token($token=null)
    {
        if (!$token) {
            $token = $this->__token['access_token'];
        }

    }

    public function get_userdata($token=null)
    {
        if (!$token) {
            $token = $this->__token['access_token'];
        }
        $context = $this->prepare_request([], "Authorization: Bearer $token", "GET");
        $result = file_get_contents($this->decode_url, false, $context);
        if ($result) {
            $result = json_decode($result, true);
            if ($result['character']) {
                $url = $result['character']['href'];
                echo $result['character']['href'];
                return json_decode(file_get_contents($url), true);
            }
        }
    }

    protected function prepare_request($data, $authhorization, $method = 'POST')
    {
        $options = array(
            'http' => array(
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n" .
                    "$authhorization\r\n",
                'method' => $method,
                'content' => http_build_query($data),
                'ignore_errors' => true
            )
        );
        $context = stream_context_create($options);
        return $context;
    }
}